var fs = require("fs")
var path = require('path')
function cv(v: any) {
    return path.resolve(__dirname, v)
}
// 清空之前的所有文件

var key: any = ''
function init(msg, surfaceName, name) {
    key = { msg, surfaceName, name }
    try {
        fs.mkdirSync(`./DaveFile/database/markdown/${surfaceName}`)
    } catch (error) {

    }
    listFs(msg, surfaceName, name)
    return {
        key
    }
}
function listFs(msg, surfaceName, name) {
    var list = [
        {
            type: "add", name: "新增"
        },
        {
            type: "query", name: "查询详情"
        },
        {
            type: "queryList", name: "查询所有",as:"querylist"
        },
        {
            type: "delete", name: "删除"
        },
        {
            type: "update", name: "更新"
        },
    ]
    list.map(s =>{
        var msg = title({ type: s.type, name: s.name,as:s.as })
        fs.writeFileSync(cv(`../DaveFile/database/markdown/${surfaceName}/${s.type}.md`), msg)
    })
}
function title(props) {
    var list = key.msg.filter(s => s[props.as || props.type] === true)
    return `
**简要描述：** 
- ${key.name}${props.name}

**请求URL：** 
- ${key.surfaceName}/${props.type}
  
**请求方式：**
- POST

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
${list.map(s => {
        return `|${s.Field} |否  |string |${s.Comment}   |` + '\n'
    }).join('')}
${renderGoBack(props, list)}

**备注** 


 `
}
function main() {
    var key = {
        "error_code": 0,
        "data": {
            "uid": "1",
            "username": "12154545",
            "name": "吴系挂",
            "groupid": 2,
            "reg_time": "1436864169",
            "last_login_time": "0",
        }
    }
    return "```\n" + JSON.stringify(key, null, 4) + "         \n```"
}
// 特殊处理返回
function renderGoBack(props, list) {
    return {
        'add': `
**返回示例**
 ${main()}
**返回参数说明** 

|参数名|类型|说明|
|:-----  |:-----|-----                           |
|status |是  |string | 200成功 501异常  |
|data |是  |Object| '' |
${list.map(s => {
            return `|data.${s.Field} |是  |string |${s.Comment}   |` + '\n'
        }).join('')}
`,
        'delete': `
**返回示例**
${main()}
**返回参数说明** 

|参数名|类型|说明|
|:-----  |:-----|-----                           |
|status |是  |string | 200成功 501异常  |
|data |是  |Object| '' |
`,
        'post': `
**返回示例**
${main()}
**返回参数说明** 

|参数名|类型|说明|
|:-----  |:-----|-----                           |
|status |是  |string | 200成功 501异常  |
|data |是  |Object| '' |
${list.map(s => {
            return `|data.${s.Field} |是  |string |${s.Comment}   |` + '\n'
        }).join('')}`,
        'queryList': `
 **返回示例**
 ${main()}
**返回参数说明** 

|参数名|类型|说明|
|:-----  |:-----|-----                           |
|status |是  |string | 200成功 501异常  |
|data |是  |Object| '' |
${list.map(s => {
            return `|data.${s.Field} |是  |string |${s.Comment}   |` + '\n'
        }).join('')}`,
        'update': `
 **返回示例**
 ${main()}
**返回参数说明** 

|参数名|类型|说明|
|:-----  |:-----|-----                           |
|status |是  |string | 200成功 501异常  |
|data |是  |Object| '' |`,
        'query': `
 **返回示例**
 ${main()}
**返回参数说明** 

|参数名|类型|说明|
|:-----  |:-----|-----                           |
|status |是  |string | 200成功 501异常  |
|data |是  |Object| '' |
${list.map(s => {
            return `|data.${s.Field} |是  |string |${s.Comment}   |` + '\n'
        }).join('')}`
    }[props.type]
}
module.exports = init